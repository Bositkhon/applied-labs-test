<?php


namespace Neuffer\FileHandler;


interface ActionInterface
{
    public function setLogFileHandler(FileHandler $fileHandler);

    public function setSourceFileHandler(FileHandler $fileHandler);

    public function setResultFileHandler(FileHandler $fileHandler);
}