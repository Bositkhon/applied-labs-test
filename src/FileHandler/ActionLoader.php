<?php

namespace Neuffer\FileHandler;

/**
 * Class ActionLoader
 * @method
 * @package src\FileHandler
 */
class ActionLoader
{
    /**
     * @var string
     */
    private $actionName;

    /**
     * @var AbstractAction
     */
    private $action;

    public function __construct(string $action)
    {
        $className = sprintf('%sAction', ucfirst($action));
        $classPath = __NAMESPACE__. '\\' . $className;

        if (!class_exists($classPath)) {
            throw new \Exception("Wrong action is selected");
        }

        $this->actionName = $action;

        $this->action = new $classPath();
    }

    public function setSourceFileHandler(FileHandler $fileHandler)
    {
        $this->action->setSourceFileHandler($fileHandler);
    }

    public function setResultFileHandler(FileHandler $fileHandler)
    {
        $this->action->setResultFileHandler($fileHandler);
    }

    public function setLogFileHandler(FileHandler $fileHandler)
    {
        $this->action->setLogFileHandler($fileHandler);
    }

    /**
     * @return AbstractAction
     * @throws \Exception
     */
    public function getAction()
    {
        return $this->action;
    }
}