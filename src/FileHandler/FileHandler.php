<?php


namespace Neuffer\FileHandler;


class FileHandler
{
    protected $filePath;

    private $buffer = [];

    private $fileResource;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function isSet(): bool
    {
        return !empty($this->filePath);
    }

    public function deleteFile()
    {
        if (file_exists($this->filePath)) {
            unlink($this->filePath);
        }
    }

    public function pushBuffer(string $line)
    {
        array_push($this->buffer, $line);
    }

    public function getFile()
    {
        if (!$this->fileResource) {
            if (empty($this->filePath) || (!$this->fileResource = fopen($this->filePath, 'r'))) {
                throw new \Exception("Can't open source file");
            }
        }
        return $this->fileResource;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function writeBuffer()
    {
        file_put_contents($this->filePath, $this->buffer, FILE_APPEND);
        $this->flushBuffer();
    }

    public function flushBuffer()
    {
        $this->buffer = [];
    }
}