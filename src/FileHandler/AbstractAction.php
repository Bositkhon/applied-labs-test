<?php

namespace Neuffer\FileHandler;

/**
 * Class AbstractAction
 */
abstract class AbstractAction implements ActionInterface
{
    /**
     * A service that operates with source file
     * @var FileHandler
     */
    protected $logFileHandler;

    /**
     * A service that operates with source file
     * @var FileHandler
     */
    protected $sourceFileHandler;

    /**
     * A service that operates with file to store results
     * @var FileHandler
     */
    protected $resultFileHandler;

    /**
     * The name of the action to be executed
     * @var string
     */
    protected $actionName;

    /**
     * execute operations
     * @throws \Exception
     */
    public function execute() : void
    {
        if (!$this->resultFileHandler->isSet()) {
            throw new \Exception("Result file is not set");
        }

        if (!$this->logFileHandler->isSet()) {
            throw new \Exception("Log file is not set");
        }

        if (!$this->sourceFileHandler->isSet()) {
            throw new \Exception("Source file is not set");
        }

        $this->logFileHandler->deleteFile();
        $this->resultFileHandler->deleteFile();

        $sourceFileResource = $this->sourceFileHandler->getFile();

        $this->logFileHandler->pushBuffer(sprintf("Started %s operation", $this->actionName));

        while (($data = fgetcsv($sourceFileResource, 1000, ";"))) {
            $a = $this->prepareNumber($data[0]);
            $b = $this->prepareNumber($data[1]);

            if ($this->isGood($a, $b)) {
                $result = $this->result($a, $b);
                $data = implode(";", [$a, $b, $result]) . "\r\n";
                $this->resultFileHandler->pushBuffer($data);
            } else {
                $this->logFileHandler->pushBuffer("numbers ".$a . " and ". $b." are wrong \r\n");
            }

            $this->resultFileHandler->writeBuffer();
            $this->logFileHandler->writeBuffer();
        }

        $this->logFileHandler->pushBuffer(sprintf("Finish %s operation", $this->actionName));

        $this->resultFileHandler->writeBuffer();

        $this->logFileHandler->writeBuffer();
    }

    public function setLogFileHandler(FileHandler $fileHandler)
    {
        $this->logFileHandler = $fileHandler;
    }

    public function setResultFileHandler(FileHandler $fileHandler)
    {
        $this->resultFileHandler = $fileHandler;
    }

    public function setSourceFileHandler(FileHandler $fileHandler)
    {
        $this->sourceFileHandler = $fileHandler;
    }

    /**
     * prepare number before action
     * @param string $value
     * @return int
     */
    protected function prepareNumber($value) : int
    {
        $value = trim($value);
        $value = intval($value);
        return $value;
    }

    abstract protected function isGood(int $a, int $b);
    abstract protected function result(int $a, int $b);
}